
const mapping = {
  '.': '!.',
  ',': '.!',
  '+': '..',
  '-': '!!',
  '>': '.?',
  '<': '?.',
  '[': '!?',
  ']': '?!',
}

export function brainfuckToOok(instructions: string, short: boolean = false): string {
  const rawOok = instructions.replace(/[^]/g, l => (mapping[l] || ''))
  const ook = rawOok.replace(/(.)/g, `${short ? '' : 'Ook'}$1 `)
  return ook.slice(0, -1)
}
