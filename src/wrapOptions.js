import wordwrap from './wordwrap'

const ID = <T>(x: T): T => x

export default {
  'one line': ID,
  'wrap at 80': wordwrap,
}
