
type Pointer = number;
type Value = number;

export type Machine = {
  input: string,
  inputPointer: Pointer,
  instructionPointer: Pointer,
  instructions: string,
  memory: Value[],
  memoryPointer: Pointer,
  output: string,
}

const instructionRunners = {
  '.': (machine: Machine) => {
    appendOutput(machine, currentMemory(machine))
    machine.instructionPointer++
  },
  ',': (machine: Machine) => {
    const readInput = currentInput(machine)
    if (readInput) {
      setCurrentMemory(machine, readInput)
      machine.inputPointer++
    }
    machine.instructionPointer++
  },
  '+': (machine: Machine) => {
    modifyCurrentMemory(machine, value => value + 1)
    machine.instructionPointer++
  },
  '-': (machine: Machine) => {
    modifyCurrentMemory(machine, value => value - 1)
    machine.instructionPointer++
  },
  '>': (machine: Machine) => {
    machine.memoryPointer++
    while (machine.memoryPointer >= machine.memory.length) {
      machine.memory.push(0)
    }
    machine.instructionPointer++
  },
  '<': (machine: Machine) => {
    machine.memoryPointer--
    while (machine.memoryPointer < 0) {
      machine.memory.unshift(0)
      machine.memoryPointer++
    }
    machine.instructionPointer++
  },
  '[': (machine: Machine) => {
    if (currentMemory(machine) === 0) {
      walkToMatchingBracket(machine, ']')
    }
    machine.instructionPointer++
  },
  ']': (machine: Machine) => {
    if (currentMemory(machine) !== 0) {
      walkToMatchingBracket(machine, '[')
    }
    machine.instructionPointer++
  },
}

const bracketValues = {
  '[': -1,
  ']': 1,
}

function walkToMatchingBracket(machine: Machine, bracket: string) {
  const bracketValue = bracketValues[bracket]
  let bracketCount = 0
  machine.instructionPointer += bracketValue
  let instruction
  while ((instruction = currentInstruction(machine)) && bracketCount !== -1) {
    const currentBracketValue = bracketValues[instruction]
    if (currentBracketValue) {
      bracketCount -= currentBracketValue * bracketValue
    }
    machine.instructionPointer += bracketValue
  }
  machine.instructionPointer -= bracketValue
}

function currentInput(machine: Machine): ?Value {
  if (machine.input[machine.inputPointer]) {
    return machine.input[machine.inputPointer].charCodeAt(0)
  }
}

function appendOutput(machine: Machine, value: Value) {
  machine.output += String.fromCharCode(value)
}

function currentInstruction(machine: Machine): ?string {
  return machine.instructions[machine.instructionPointer]
}

function currentMemory(machine: Machine): Value {
  return machine.memory[machine.memoryPointer]
}

function setCurrentMemory(machine: Machine, value: number) {
  machine.memory[machine.memoryPointer] = value
}

function modifyCurrentMemory(machine: Machine, mapper: Value => Value) {
  setCurrentMemory(machine, mapper(currentMemory(machine)))
}

function handleComments(machine: Machine) {
  let instruction
  while ((instruction = currentInstruction(machine)) && isComment(instruction)) {
    machine.instructionPointer++
  }
}

export function create(input: string, instructions: string): Machine {
  return {
    input,
    inputPointer: 0,
    instructionPointer: 0,
    instructions,
    memory: [0],
    memoryPointer: 0,
    output: '',
  }
}

export function isComment(instruction: string): boolean {
  return !instructionRunners[instruction]
}

export function isDone(machine: Machine): boolean {
  return !currentInstruction(machine)
}

const NOT_COMMANDS = /[^.,\-+[\]<>]/g
const NOOPS = /<>|><|-\+|\+-/g

export function minify(instructions: string): string {
  let minifiedInstructions = instructions.replace(NOT_COMMANDS, '')
  while (minifiedInstructions.replace(NOOPS, '') !== minifiedInstructions) {
    minifiedInstructions = minifiedInstructions.replace(NOOPS, '')
  }
  return minifiedInstructions
}

export function restart(machine: Machine) {
  machine.inputPointer = 0
  machine.instructionPointer = 0
  machine.memory = [0]
  machine.memoryPointer = 0
  machine.output = ''
}

export function run(machine: Machine): string {
  while (step(machine)) {}
  return machine.output
}

export function step(machine: Machine): boolean {
  handleComments(machine)
  const instruction = currentInstruction(machine)
  if (instruction) {
    instructionRunners[instruction](machine)
    handleComments(machine)
    return true
  }
  return false
}
