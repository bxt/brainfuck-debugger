import React from 'react'

import Button from './Button'

export default function SelectButton<T>(props: {
  options: {[label: string]: T},
  onChange: T => void,
  value: T,
}) {
  const optionButtons = Object.keys(props.options).map(label => {
    const value = props.options[label]
    const active = props.value === value
    const key = label
    const onClick = () => props.onChange(value)

    return (
      // Workaround for this bug: https://github.com/yannickcr/eslint-plugin-react/issues/613
      // eslint-disable-next-line react/jsx-key
      <Button {...{active, key, onClick, value}}>
        { label }
      </Button>
    )
  })

  return (
    <span className="select-button">
      { optionButtons }
    </span>
  )
}
