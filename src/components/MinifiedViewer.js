import React from 'react'

import SelectButton from './SelectButton'

import { minify } from '../brainfuck'
import wrapOptions from '../wrapOptions'

export default class MinifiedViewer extends React.Component {
  state: {
    wrap: string => string,
  } = {
    wrap: wrapOptions['one line'],
  }

  props: {
    instructions: string
  }

  render() {
    return (
      <div>
        <SelectButton
          onChange={wrap => this.setState({wrap})}
          options={wrapOptions}
          value={this.state.wrap}
        />
        <div className="code-view">
          {this.state.wrap(minify(this.props.instructions))}
        </div>
      </div>
    )
  }
}
