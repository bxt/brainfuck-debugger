import React from 'react'

import { isComment } from '../brainfuck'

function formatInstructions(instructions) {
  let key = 0
  let start = 0
  let comment = null
  let elements = []
  for (let i = 0; i < instructions.length; i++) {
    let currentLetter = instructions[i]
    let nowComment = isComment(currentLetter)
    if (nowComment !== comment) {
      const className = `code-view__${comment ? 'comment' : 'code'}`
      elements.push(<span {...{className, key}}>{instructions.substring(start, i)}</span>)
      comment = nowComment
      start = i
      key++
    }
  }
  const className = `code-view__${comment ? 'comment' : 'code'}`
  elements.push(<span {...{className, key}}>{instructions.substring(start)}</span>)
  return elements
}

type Props = {
  instructionPointer: number,
  instructions: string,
}

export default function CodeViewer({instructionPointer, instructions}: Props) {
  const instructionsBefore = instructions.substring(0, instructionPointer)
  const instructionsAt = instructions[instructionPointer]
  const instructionsAfter = instructions.substring(instructionPointer + 1)

  return (
    <div className="code-view">
      {formatInstructions(instructionsBefore)}
      <span className="code-view__current">{instructionsAt}</span>
      {formatInstructions(instructionsAfter)}
    </div>
  )
}
