import React from 'react'

import Button from './Button'
import SelectButton from './SelectButton'

export default class Step extends React.Component {
  state = {
    stepSize: 1,
  }

  props: {onStep: number => void}

  render() {
    return (
      <span>
        <Button onClick={() => this.props.onStep(this.state.stepSize)}>step</Button>
        <SelectButton
          onChange={stepSize => this.setState({stepSize})}
          options={{'1': 1, '10': 10, '100': 100, '1k': 1000}}
          value={this.state.stepSize}
        />
      </span>
    )
  }
}
