import React from 'react'

type Props = {
  instructions: string,
  onChange: string => void
}

export default function CodeEditor(props: Props) {
  const {instructions, onChange} = props

  return (
    <div>
      <textarea value={instructions} onChange={(event) => onChange(event.target.value)} />
    </div>
  )
}
