import React from 'react'

import SelectButton from './SelectButton'

const CELL_RENDERERS : {[label: string]: number => string} = {
  hex: value => value.toString(16),
  dec: value => `${value}`,
  ascii: value => {
    return String
      .fromCharCode(value)
      .replace(/[\0-\x20\x7F-\x9F\xAD]/, c => encodeURIComponent(c))
  },
}

export default class MemoryViewer extends React.Component {
  state = {
    cellRenderer: CELL_RENDERERS.hex,
  }

  props: {
    memory: number[],
    memoryPointer: number,
  }

  render() {
    const cells = this.props.memory.map((value, address) => {
      const active = address === this.props.memoryPointer
      const className = `memory__cell${active ? ' memory__cell--active' : ''}`
      const key = address
      const title = Object.keys(CELL_RENDERERS).map(label => {
        return `${label}: ${CELL_RENDERERS[label](value)}`
      }).join(', ')

      return (
        // Workaround for this bug: https://github.com/yannickcr/eslint-plugin-react/issues/613
        // eslint-disable-next-line react/jsx-key
        <div {...{className, key, title}}>
          {this.state.cellRenderer(value)}
        </div>
      )
    })

    return (
      <div>
        <SelectButton
          onChange={cellRenderer => this.setState({cellRenderer})}
          options={CELL_RENDERERS}
          value={this.state.cellRenderer}
        />
        <div className="memory">
          {cells}
          <br style={{clear: 'left'}} />
        </div>
      </div>
    )
  }
}
