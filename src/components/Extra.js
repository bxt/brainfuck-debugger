import React from 'react'

import MinifiedViewer from './MinifiedViewer'
import OokViewer from './OokViewer'

import { brainfuckToOok } from '../ook'

export default function Extra({instructions}: {instructions: string}) {
  return (
    <div>
      <h2>What is Brainfuck?</h2>
      <p>Although it doesn&apos;t sound like much fun, <a href="https://en.wikipedia.org/wiki/Brainfuck">Brainfuck</a> is an amusing programing language. It only has eight commands <code>+-&lt;&gt;,.[]</code> and is Turing-complete. Actually it&apos;s pretty similar to a Turing-Machine. With <code>&lt;&gt;</code> you can move the pointer back and forth, with <code>+-</code> in- and decrease the value pointed to, with <code>,.</code> read and write input, and <code>[]</code> forms a <code>while</code>-loop.</p>
      <p>Since it&apos;s so simple, it&apos;s very easy to create compilers, interpreters and other tools for it. Also, you can really focus on your algorithms and take a break from all the OOP-fu.</p>
      <p>It is of course not intended for practical use, but don&apos;t let this fool you: you can still code <a href="https://github.com/matslina/awib">amazing stuff</a> with it.</p>

      <h2>How to use this debugger?</h2>
      <p>There&apos;s a code pane on the left where you can enter your program code. Also there&apos;s an input pane on the right, where you can enter some input. Below the input pane, theres the output. Characters will appear once you start your program, by clicking on the start button. It will expand into further controls to control execution. On top of the right column, there&apos;s the memory view, showing you what&apos;s in your memory cells at the moment.</p>

      <h2>Enivronment</h2>
      <p>Since Brainfuck ist not a standardized language, here&apos;s the assumptions we make:</p>
      <ul>
        <li>When the program reaches <code>EOF</code> (the end of your input textarea) a subsequent read <code>,</code> will not change the cell value. If your code expects <code>0</code> on <code>EOF</code> you have to zero the cell manually like this: <code>[-],</code>.</li>
        <li>Line breaks are just one line feed (<code>\n</code>, NL) read as decimal 10.</li>
        <li>There&apos;s no integer wraparound. The cell values are just numbers from JS, so the highest value is {Number.MAX_SAFE_INTEGER}. If you reach this it will break and try to use floats.</li>
        <li>The memory size is only limited by the maximum size of a JavaScript array, so 2<sup>32</sup>-1 elements at most.</li>
        <li>All characters read as unicode code points. So if your input is &#9822; a <code>,</code> will put 9822 into the memory cell. Emoji may be a bit problematic, because they may be more then one code point.</li>
      </ul>

      <h2>Can you minify my code?</h2>
      <p>Sure thing:</p>
      <MinifiedViewer {...{instructions}} />

      <h2>But I&apos;m an Orangutan!</h2>
      <p>
        {brainfuckToOok('++++++++[>++++++++++>++++++++++++>++++<<<-]>--.>+++++++++++++++.>.<.---------..-.+++++++++.+++++.--------------.>+.')}
      </p>
      <OokViewer {...{instructions}} />
    </div>
  )
}
