import React from 'react'

import Button from './Button'

type Props = {
  onTick: () => void,
};

export default class Player extends React.Component {
  props: Props;
  timerID: number;

  state = {
    running: false,
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }

  tick() {
    this.props.onTick()
  }

  start() {
    this.timerID = setInterval(() => this.tick(), 10)
    this.setState({running: true})
  }

  stop() {
    clearInterval(this.timerID)
    this.setState({running: false})
  }

  toggle = () => {
    if (this.state.running) {
      this.stop()
    } else {
      this.start()
    }
  }

  render() {
    return (
      <Button onClick={this.toggle} active={this.state.running}>play</Button>
    )
  }
}
