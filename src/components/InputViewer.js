import React from 'react'

type Props = {
  input: string,
  inputPointer: number,
}

export default function InputViewer({input, inputPointer}: Props) {
  const inputBefore = input.substring(0, inputPointer)
  const inputAt = input[inputPointer]
  const inputAfter = input.substring(inputPointer + 1)

  return (
    <div className="code-view code-view--small">
      {inputBefore}
      <span className="code-view__current">{inputAt}</span>
      {inputAfter}
    </div>
  )
}
