import React from 'react'

export default function InputEditor(props: {input: string, onChange: string => void}) {
  const {input, onChange} = props

  return (
    <div>
      <textarea
        value={input}
        onChange={(event) => onChange(event.target.value)}
        className="small" />
    </div>
  )
}
