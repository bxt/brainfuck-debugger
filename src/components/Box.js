import React from 'react'

export default function Box({children}: {children?: React$Element<any>}) {
  return (
    <div className="box">
      <div className="box__inner">
        { children }
      </div>
    </div>
  )
}
