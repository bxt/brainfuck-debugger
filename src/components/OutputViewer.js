import React from 'react'

export default function OutputViewer({output}: {output: string}) {
  return (
    <div className="code-view code-view--small">
      {output}
    </div>
  )
}
