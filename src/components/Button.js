import React from 'react'

type Props = {
  children?: React$Element<any>,
  active?: boolean,
  onClick: () => void,
}

export default function Button(props: Props) {
  return (
    <a
      href="javascript:;"
      onClick={props.onClick}
      className={`button${props.active ? ' button--active' : ''}`}
    >
      { props.children }
    </a>
  )
}
