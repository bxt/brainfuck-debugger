import React from 'react'

import SelectButton from './SelectButton'

import { brainfuckToOok } from '../ook'
import wrapOptions from '../wrapOptions'

export default class OokViewer extends React.Component {
  state: {
    minify: boolean,
    wrap: string => string,
  } = {
    minify: false,
    wrap: wrapOptions['one line'],
  }

  props: {instructions: string}

  render() {
    return (
      <div>
        <SelectButton
          onChange={minify => this.setState({minify})}
          options={{full: false, minified: true}}
          value={this.state.minify}
        />
        <SelectButton
          onChange={wrap => this.setState({wrap})}
          options={wrapOptions}
          value={this.state.wrap}
        />
        <div className="code-view">
          {this.state.wrap(brainfuckToOok(this.props.instructions, this.state.minify))}
        </div>
      </div>
    )
  }
}
