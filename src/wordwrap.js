
export default function wordwrap(input: string, maxLineLength: number = 80): string {
  const regexp = new RegExp(`^$|.{1,${maxLineLength}}$|.{${maxLineLength}}`, 'gm')
  const matches = input.match(regexp);

  if (matches) {
    return matches.join(`\n`)
  } else {
    throw new Error("unexpectedly no matches");
  }
}
