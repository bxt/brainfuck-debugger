import { expect } from './helper'

import wordwrap from '../src/wordwrap'

describe('wordwrap(input: string, maxLineLength: number = 80)', function () {
  it('does nothing to a string shorter than maxLineLength', function () {
    expect(wordwrap(`123`, 4)).to.equal(`123`)
  })
  it('does nothing to a string with maxLineLength', function () {
    expect(wordwrap(`1234`, 4)).to.equal(`1234`)
  })
  it('wraps a single char after maxLineLength into a new line', function () {
    expect(wordwrap(`12345`, 4)).to.equal(`1234\n5`)
  })
  it('wraps longer lines', function () {
    expect(wordwrap(`abcdefghijk\nlmno\npqr`, 4)).to.equal(`abcd\nefgh\nijk\nlmno\npqr`)
  })
  it('preserves trailing new lines', function () {
    expect(wordwrap(`123\n`, 4)).to.equal(`123\n`)
    expect(wordwrap(`1234\n`, 4)).to.equal(`1234\n`)
    expect(wordwrap(`12345\n`, 4)).to.equal(`1234\n5\n`)
  })
  it('can handle an empty string', function () {
    expect(wordwrap(``, 4)).to.equal(``)
  })
  it('preserves empty lines', function () {
    expect(wordwrap(`a\n\nc`, 4)).to.equal(`a\n\nc`)
  })
})
