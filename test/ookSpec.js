import { expect } from './helper'

import { brainfuckToOok } from '../src/ook'

describe('ook', function () {
  describe('#brainfuckToOok(instructions)', function () {
    it('maps all the brainfuck commands', function () {
      expect(brainfuckToOok('+-><[].,')).to.equal('Ook. Ook. Ook! Ook! Ook. Ook? Ook? Ook. Ook! Ook? Ook? Ook! Ook! Ook. Ook. Ook!')
    })

    it('removes comments', function () {
      expect(brainfuckToOok('cc+cc')).to.equal('Ook. Ook.')
    })
  })

  describe('#brainfuckToOok(instructions, short = true)', function () {
    it('maps all the brainfuck commands', function () {
      expect(brainfuckToOok('+-><[].,', true)).to.equal('. . ! ! . ? ? . ! ? ? ! ! . . !')
    })

    it('removes comments', function () {
      expect(brainfuckToOok('cc+cc', true)).to.equal('. .')
    })
  })
})
