brainfuck-debugger
==================

[![build status](https://gitlab.com/bxt/brainfuck-debugger/badges/master/build.svg)](https://gitlab.com/bxt/brainfuck-debugger/commits/master)
[![coverage report](https://gitlab.com/bxt/brainfuck-debugger/badges/master/coverage.svg)](https://bxt.gitlab.io/brainfuck-debugger/coverage/lcov-report/)


A debugger for brainfuck code.

Use it here: https://bxt.gitlab.io/brainfuck-debugger/

What is Brainfuck?
------------------

Although it doesn't sound like much fun,
[Brainfuck](https://en.wikipedia.org/wiki/Brainfuck) is an amusing programing
language. It only has eight commands +-<>,.[] and is Turing-complete. Actually
it's pretty similar to a Turing-Machine. With <> you can move the pointer back
and forth, with +- in- and decrease the value pointed to, with ,. read and write
input, and [] forms a while-loop.

Since it's so simple, it's very easy to create compilers, interpreters and other
tools for it. Like this one. Also, you can really focus on your algorithms and
take a break from all the OOP-fu.

It is of course not intended for practical use, but don't let this fool you: you
can still code amazing stuff with it. And this will help.

How to use this debugger?
-------------------------

There's a code pane on the left where you can enter your program code. Also
there's an input pane on the right, where you can enter some input. Below the
input pane, theres the output. Characters will appear once you start your
program, by clicking on the start button. It will expand into further controls
to control execution. On top of the right column, there's the memory view,
showing you what's in your memory cells at the moment.

Enivronment
-----------

Since Brainfuck ist not a standardized language, here's the assumptions we make:

* When the program reaches `EOF` (the end of your input textarea) a subsequent
  read `,` will not change the cell value. If your code expects `0` on `EOF` you
  have to zero the cell manually like this: `[-],`.
* Line breaks are just one line feed (`\n`, NL) read as decimal `10`.
* There's no integer wraparound. The cell values are just numbers from JS, so
  the highest value is 9007199254740991. If you reach this it will
  break and try to use floats.
* The memory size is only limited by the maximum size of a JavaScript array,
  so 2^32-1 elements at most.
* All characters read as unicode code points. So if your input is ♞ a `,` will
  put 9822 into the memory cell.

Developing
---------

To **install** all the required packages and **compile** `index.min.js` do:

    npm install

Then open `index.html` in your browser.

If you want to **watch** for changes to recompile do:

    npm start

To run **all** tests, checks and linting do:

    npm -s test

To run the **tests** ([assertions info](http://chaijs.com/api/bdd/)) use:

    npm run -s mocha
    # or
    npm run -s mocha -- -w

To **lint** the JS run:

    $(npm bin)/eslint src/**
    # Or:
    npm run -s lint

Use `--fix` to fix some stuff automatically.

To run **flow** do:

    npm run-script -s flow

This will type-check the code.

Useful links:

* [Using flow type with react](https://flowtype.org/docs/react.html)
* [Using react with babel and ES6+](https://babeljs.io/blog/2015/06/07/react-on-es6-plus)
* [Using state in react](https://facebook.github.io/react/docs/state-and-lifecycle.html)
* [ACSII table](http://www.asciitable.com/)
